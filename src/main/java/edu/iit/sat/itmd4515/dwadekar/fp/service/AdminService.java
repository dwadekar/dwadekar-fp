/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.service;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.Admin;
import edu.iit.sat.itmd4515.dwadekar.fp.security.Group;
import edu.iit.sat.itmd4515.dwadekar.fp.security.User;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

/**
 * Used to provide services to Administrator Group
 *
 * @author dwadekar
 */
@Stateless
public class AdminService extends AbstractService<Admin> {

    /**
     * Default Constructor
     */
    public AdminService() {
        super(Admin.class);
    }

    /**
     * Returns the list of all Employee's login information
     *
     * @return
     */
    @Override
    public List<Admin> findAll() {
        return em.createNamedQuery("Admin.findAll").getResultList();
    }

    /**
     * Return the information of Single Employee by username
     *
     * @param username
     * @return
     */
    public Admin findByUsername(String username) {
        TypedQuery<Admin> query = em.createNamedQuery("Admin.findByUsername", Admin.class);
        query.setParameter("username", username);
        return query.getSingleResult();
    }

    /**
     * Delete the administrative user
     *
     * @param adminUser
     */
    public void deleteAdmin(Admin adminUser) {
        adminUser = em.getReference(Admin.class, adminUser.getId());
        User user = adminUser.getUser();
        List<Group> group = user.getUserGroups();
        for (Group grp : group) {
            Group currentGrp = em.getReference(Group.class, grp.getGroupName());
            currentGrp.getGroupMembers().remove(user);
        }

        em.remove(adminUser);
        em.remove(user);
    }

    /**
     * Update the administrative user information
     *
     * @param newAdminUser
     * @param newUser
     */
    public void updateAdmin(Admin newAdminUser, User newUser) {
        Admin currentAdmin = em.getReference(Admin.class, newAdminUser.getId());
        User currentUser = em.getReference(User.class, newUser.getUserName());
        List<Group> group = currentUser.getUserGroups();
        currentUser.setPassword("12345");
        currentUser.setNameOfUser(newUser.getNameOfUser());
        currentUser.setEmailID(newUser.getEmailID());
        List<User> userList = new ArrayList<>();
        userList.add(currentUser);
        for (Group grp : group) {
            Group currentGrp = em.getReference(Group.class, grp.getGroupName());
            currentGrp.setGroupMembers(userList);
        }
        currentAdmin.setUser(currentUser);

        em.merge(currentUser);
        em.merge(currentAdmin);
    }

    /**
     * Create administrative users in the application
     *
     * @param group
     * @param newUser
     */
    public void createAdmin(Group group, User newUser) {
        Admin admin = new Admin();
        newUser.setPassword("12345");
        newUser.addUserToGroup(group);
        em.persist(newUser);
        admin.setUser(newUser);
        em.persist(admin);
    }
}
