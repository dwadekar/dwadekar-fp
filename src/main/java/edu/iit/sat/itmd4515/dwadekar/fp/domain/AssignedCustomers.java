/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.domain;

import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * List of Assigned Customers to Relationship Manager domain class
 *
 * @author dwadekar
 */
@Entity
@Table(name = "AssignedCustomersInfo")
@NamedQueries({
    @NamedQuery(name = "AssignedCustomers.findAll", query = "select c from AssignedCustomers c")
})
public class AssignedCustomers extends CommonEntity {

    private Long customerID;
    private String customerName;

    /**
     * Default constructor with no parameters for class AssignedCustomers
     */
    public AssignedCustomers() {
    }

    /**
     * Constructor with the following parameters passed to it
     *
     * @param customerID
     * @param customerName
     */
    public AssignedCustomers(Long customerID, String customerName) {
        this.customerID = customerID;
        this.customerName = customerName;
    }

    /**
     * Returns the Customer ID set for the given object
     *
     * @return
     */
    public Long getCustomerID() {
        return customerID;
    }

    /**
     * Set the Customer ID for the passed object
     *
     * @param customerID
     */
    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    /**
     * Returns the Customer Name set for the given object
     *
     * @return
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Set the Customer Name for the passed object
     *
     * @param customerName
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Override
    public String toString() {
        return String.format("%15s%20s", getCustomerID() + " |", getCustomerName() + " |");
    }

}
