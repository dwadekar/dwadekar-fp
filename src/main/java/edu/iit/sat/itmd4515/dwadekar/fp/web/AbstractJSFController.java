/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.web;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;

/**
 * Abstract class for various controller classes
 *
 * @author dwadekar
 */
public abstract class AbstractJSFController {

    /**
     * Initialization of FacesContext
     */
    protected FacesContext facesContext;

    /**
     * Initialization of FACES_REDIRECT
     */
    protected static final String FACES_REDIRECT = "?faces-redirect=true";

    /**
     * Post Construct method for setting facesContext
     */
    @PostConstruct
    protected void postConstruct() {
        facesContext = FacesContext.getCurrentInstance();
    }
}
