/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.domain;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Account information of a Customers domain class
 *
 * @author dwadekar
 */
@Entity
@Table(name = "Account_Info")
@NamedQueries({
    @NamedQuery(name = "AccountInfo.findByID", query = "select a from AccountInfo a where a.customerID = :custID"),
    @NamedQuery(name = "AccountInfo.findAll", query = "select a from AccountInfo a")
})
public class AccountInfo extends CommonEntity {

    private Long accountNo;
    private Long customerID;
    private String accountType;
    private String accountDescription;
    private Float availableBalance;
    private String currency;
    @OneToMany
    @JoinTable(name = "Accountwise_Txn_Info", joinColumns = @JoinColumn(name = "accountInfo_fk"), inverseJoinColumns = @JoinColumn(name = "TxnInfo_fk"))
    private List<TransactionInfo> transactions;

    /**
     * Default constructor with no parameters for class AccountInfo
     *
     */
    public AccountInfo() {
    }

    /**
     * Constructor with the following parameters passed to it
     *
     * @param accountNo
     * @param customerID
     * @param accountType
     * @param accountDescription
     * @param availableBalance
     * @param currency
     * @param creationDate
     */
    public AccountInfo(Long accountNo, Long customerID, String accountType, String accountDescription, Float availableBalance, String currency, Date creationDate) {
        this.accountNo = accountNo;
        this.customerID = customerID;
        this.accountType = accountType;
        this.accountDescription = accountDescription;
        this.availableBalance = availableBalance;
        this.currency = currency;
        this.creationDate = creationDate;
    }

    /**
     * Set the ID for the passed object in the class
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Returns set Account No for the given object
     *
     * @return
     */
    public Long getAccountNo() {
        return accountNo;
    }

    /**
     * Set the Account No for the passed object
     *
     * @param accountNo
     */
    public void setAccountNo(Long accountNo) {
        this.accountNo = accountNo;
    }

    /**
     * Returns the customer ID set for the given object
     *
     * @return
     */
    public Long getCustomerID() {
        return customerID;
    }

    /**
     * Set the Customer ID for the passed object
     *
     * @param customerID
     */
    public void setCustomerID(Long customerID) {
        this.customerID = customerID;
    }

    /**
     * Returns the Account type set for the given object
     *
     * @return
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Set the Account Type for the passed object
     *
     * @param accountType
     */
    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    /**
     * Returns the Account Description for the given object
     *
     * @return
     */
    public String getAccountDescription() {
        return accountDescription;
    }

    /**
     * Set the Account Description for the passed object
     *
     * @param accountDescription
     */
    public void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }

    /**
     * Returns the Available Balance for the given object
     *
     * @return
     */
    public Float getAvailableBalance() {
        return availableBalance;
    }

    /**
     * Set the Available balance for the passed object
     *
     * @param availableBalance
     */
    public void setAvailableBalance(Float availableBalance) {
        this.availableBalance = availableBalance;
    }

    /**
     * Returns the Currency set for the given object
     *
     * @return
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Set the currency for the passed object
     *
     * @param currency
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * Set the date of creation of account for the passed object
     *
     * @param creationDate
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Returns the collection of transactions happened for the given passed
     * customers or account
     *
     * @return
     */
    public List<TransactionInfo> getTransactions() {
        return transactions;
    }

    /**
     * Set the collection of transactions happened for passed transactions
     *
     * @param transactions
     */
    public void setTransactions(List<TransactionInfo> transactions) {
        this.transactions = transactions;
    }

    @Override
    public String toString() {
        return String.format("%22s%14s%20s%22s%20s%10s%30s", getAccountNo() + " |", getCustomerID() + " |", getAccountType() + " |", getAccountDescription() + " |", new DecimalFormat("#.##").format(getAvailableBalance()) + " |", getCurrency() + " |", getCreationDate() + " |");
    }

}
