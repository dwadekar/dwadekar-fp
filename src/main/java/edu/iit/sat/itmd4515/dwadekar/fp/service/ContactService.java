/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.service;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.Contact;
import java.util.List;
import javax.ejb.Stateless;

/**
 * Used for providing Contact service details of Customer
 *
 * @author dwadekar
 */
@Stateless
public class ContactService extends AbstractService<Contact> {

    /**
     * Default Constructor
     */
    public ContactService() {
        super(Contact.class);
    }

    /**
     * Returns the all Customer's Contact information
     *
     * @return
     */
    @Override
    public List<Contact> findAll() {
        return em.createNamedQuery("Contact.findAll", Contact.class).getResultList();
    }
}
