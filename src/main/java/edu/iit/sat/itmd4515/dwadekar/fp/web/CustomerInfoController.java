/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.web;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.AccountInfo;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.Address;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.Contact;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.Customer;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.ProofOfDocuments;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.TransactionInfo;
import edu.iit.sat.itmd4515.dwadekar.fp.service.AccountInfoService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.AddressService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.BankService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.ContactService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.ProofOfDocService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.TransactionInfoService;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Used for Controlling Customer's operations
 *
 * @author dwadekar
 */
@Named
@RequestScoped
public class CustomerInfoController extends AbstractJSFController {

    private static final Logger LOG = Logger.getLogger(CustomerInfoController.class.getName());

    private List<Customer> customersList;
    private Customer custInfo;
    private Address addressInfo;
    private Contact contactInfo;
    private ProofOfDocuments docsProof;
    private AccountInfo accountInfo;
    private AccountInfo newAccountInfo;
    private TransactionInfo txnByCust;
    private TransactionInfo newTransaction;
    private List<TransactionInfo> transactionList;
    private List<TransactionInfo> newTransactionList;
    private List<AccountInfo> accountList;

    @EJB
    private BankService customerService;
    @EJB
    private AddressService addressService;
    @EJB
    private ContactService contactService;
    @EJB
    private ProofOfDocService proofDocService;
    @EJB
    private AccountInfoService accountInfoService;
    @EJB
    private TransactionInfoService txnInfoService;
    @Inject
    LoginController loginController;

    /**
     * Default Constructor
     */
    public CustomerInfoController() {
    }

    /**
     * Post Constructor
     */
    @Override
    @PostConstruct
    protected void postConstruct() {
        txnByCust = new TransactionInfo();
        accountInfo = new AccountInfo();
        newAccountInfo = new AccountInfo();
        custInfo = customerService.findCustomer(loginController.getRemoteUser());
        try {
            addressInfo = custInfo.getAddress();
        } catch (NullPointerException ex) {
            LOG.info("No address details of customer found!! Null pointer exception caught..");
        }
        try {
            contactInfo = custInfo.getContact();
        } catch (NullPointerException ex) {
            LOG.info("No contact details of customer found!! Null pointer exception caught..");
        }
        try{
            accountList = accountInfoService.findAccounts(custInfo.getId());
        }catch(NullPointerException ex){
            LOG.info("No account details of customer found!! Null pointer exception caught..");
        }
        super.postConstruct();
    }

    private void refreshTransactionList() {
        accountList = accountInfoService.findAccounts(custInfo.getId());
    }

    /**
     * Used for performing transaction with Customer ID
     *
     * @param accountInfo
     * @return
     */
    public String doPerformTransaction(AccountInfo accountInfo) {
        LOG.info("Preparing to create a new transaction in Account: " + accountInfo.toString());
        txnByCust = new TransactionInfo();
        this.accountInfo = accountInfo;
        try {
            if (this.accountInfo.getAvailableBalance() < 0) {
                return "errorTxn";
            } else {
                return "performTxn";
            }
        } catch (NullPointerException ex) {
            LOG.info("No account details found for performing transaction");
            return "errorPerformTxn";
        }
    }

    /**
     * Execute the create operation transaction
     *
     * @return
     */
    public String executeTransaction() {
        LOG.info("Preparing to execute transaction : " + txnByCust.toString());

        Float chkBalance = accountInfo.getAvailableBalance() - txnByCust.getAmount();

        if (chkBalance < 0) {
            return "errorTxn";
        } else {
            this.newTransaction = txnByCust;
            newAccountInfo = txnInfoService.create(txnByCust, accountInfo);

            // refresh the collection
            refreshTransactionList();
            return "success";
        }
    }
    
    /**
     * Used to display the Account information
     *
     * @param accountInfo
     * @return
     */
    public String doShowAccountInfo(AccountInfo accountInfo) {
        LOG.info("Preparing to display Account info " + accountInfo.toString());
        
        this.accountInfo = accountInfo;
        return "viewAccountInfo";
    }
    
    /**
     * Used to display transaction performed in a selected account
     *
     * @param accountInfo
     * @return
     */
    public String doShowTransactionSummary(AccountInfo accountInfo) {
        LOG.info("Preparing to display Transaction info in an Account " + accountInfo.toString());
        
        this.accountInfo = accountInfo;
        transactionList = this.accountInfo.getTransactions();
        
        return "viewTransactionSummary";
    }

    /**
     * Get the value of customers
     *
     * @return the value of customers
     */
    public List<Customer> getCustomers() {
        return customersList;
    }

    /**
     * Set the value of customers
     *
     * @param customersList
     */
    public void setCustomers(List<Customer> customersList) {
        this.customersList = customersList;
    }

    /**
     * Get the value of Customer
     *
     * @return the value of Customer
     */
    public Customer getCustomer() {
        return custInfo;
    }

    /**
     * Set the value of Customer
     *
     * @param custInfo
     */
    public void setCustomer(Customer custInfo) {
        this.custInfo = custInfo;
    }

    /**
     * Returns the Address of a customer
     *
     * @return
     */
    public Address getAddressInfo() {
        return addressInfo;
    }

    /**
     * Set the Address of a customer
     *
     * @param addressInfo
     */
    public void setAddressInfo(Address addressInfo) {
        this.addressInfo = addressInfo;
    }

    /**
     * Returns the contact of a customer
     *
     * @return
     */
    public Contact getContactInfo() {
        return contactInfo;
    }

    /**
     * Set the contact of a customer
     *
     * @param contactInfo
     */
    public void setContactInfo(Contact contactInfo) {
        this.contactInfo = contactInfo;
    }

    /**
     * Returns the Proof of Document of a customer
     *
     * @return
     */
    public ProofOfDocuments getDocsProof() {
        return docsProof;
    }

    /**
     * Set the Proof of Document of a customer
     *
     * @param docsProof
     */
    public void setDocsProof(ProofOfDocuments docsProof) {
        this.docsProof = docsProof;
    }

    /**
     * Returns the Account information of a customer
     *
     * @return
     */
    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    /**
     * Set the Account information of a customer
     *
     * @param accountInfo
     */
    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    /**
     * Returns the list of Transaction perform by a customer's
     *
     * @return
     */
    public List<TransactionInfo> getTransactionList() {
        return transactionList;
    }

    /**
     * Set the list of Transaction perform by a customer's
     *
     * @param transactionList
     */
    public void setTransactionList(List<TransactionInfo> transactionList) {
        this.transactionList = transactionList;
    }

    /**
     * Returns the list of transaction perform by Customer
     *
     * @return
     */
    public TransactionInfo getTxnByCust() {
        return txnByCust;
    }

    /**
     * Set the transaction by Customer
     *
     * @param txnByCust
     */
    public void setTxnByCust(TransactionInfo txnByCust) {
        this.txnByCust = txnByCust;
    }

    /**
     * Returns the account information of a customer
     *
     * @return
     */
    public AccountInfo getNewAccountInfo() {
        return newAccountInfo;
    }

    /**
     * Set the account information of a customer
     *
     * @param newAccountInfo
     */
    public void setNewAccountInfo(AccountInfo newAccountInfo) {
        this.newAccountInfo = newAccountInfo;
    }

    /**
     * Returns the Transaction perform by customer
     *
     * @return
     */
    public TransactionInfo getNewTransaction() {
        return newTransaction;
    }

    /**
     * Set the transaction perform by a customer
     *
     * @param newTransaction
     */
    public void setNewTransaction(TransactionInfo newTransaction) {
        this.newTransaction = newTransaction;
    }

    /**
     * Returns the list of Transactions perform by a customer
     *
     * @return
     */
    public List<TransactionInfo> getNewTransactionList() {
        return newTransactionList;
    }

    /**
     * Set the list of Transactions perform by a customer
     *
     * @param newTransactionList
     */
    public void setNewTransactionList(List<TransactionInfo> newTransactionList) {
        this.newTransactionList = newTransactionList;
    }

    /**
     * Returns the list of customer Accounts
     *
     * @return
     */
    public List<AccountInfo> getAccountList() {
        return accountList;
    }

    /**
     * Set the list of customer Accounts
     *
     * @param accountList
     */
    public void setAccountList(List<AccountInfo> accountList) {
        this.accountList = accountList;
    }

}
