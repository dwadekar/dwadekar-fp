/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.service;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.CustomersLogin;
import edu.iit.sat.itmd4515.dwadekar.fp.security.Group;
import edu.iit.sat.itmd4515.dwadekar.fp.security.User;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

/**
 * Used to providing Customer services to Customers Group
 *
 * @author dwadekar
 */
@Stateless
public class CustomerService extends AbstractService<CustomersLogin> {

    /**
     * Default Constructor
     */
    public CustomerService() {
        super(CustomersLogin.class);
    }

    /**
     * Returns the all Customer's login information
     *
     * @return
     */
    @Override
    public List<CustomersLogin> findAll() {
        return em.createNamedQuery("CustomersLogin.findAll").getResultList();
    }

    /**
     * Returns the Single Customer's login information by username
     *
     * @param username
     * @return
     */
    public CustomersLogin findByUsername(String username) {
        TypedQuery<CustomersLogin> query = em.createNamedQuery("CustomersLogin.findByUsername", CustomersLogin.class);
        query.setParameter("username", username);
        return query.getSingleResult();
    }

    /**
     * Delete customer login information
     *
     * @param customer
     */
    public void deleteCustomer(CustomersLogin customer) {
        customer = em.getReference(CustomersLogin.class, customer.getId());
        User user = customer.getUser();
        List<Group> group = user.getUserGroups();
        for (Group grp : group) {
            Group currentGrp = em.getReference(Group.class, grp.getGroupName());
            currentGrp.getGroupMembers().remove(user);
        }

        em.remove(customer);
        em.remove(user);
    }

    /**
     * Update the customer login information
     *
     * @param newCustomer
     * @param newUser
     */
    public void updateCustomer(CustomersLogin newCustomer, User newUser) {
        CustomersLogin currentCustomer = em.getReference(CustomersLogin.class, newCustomer.getId());
        //User currentUser = currentCustomer.getUser();
        User currentUser = em.getReference(User.class, newUser.getUserName());
        List<Group> group = currentUser.getUserGroups();
        currentUser.setPassword("12345");
        currentUser.setNameOfUser(newUser.getNameOfUser());
        currentUser.setEmailID(newUser.getEmailID());
        List<User> userList = new ArrayList<>();
        userList.add(currentUser);
        for (Group grp : group) {
            Group currentGrp = em.getReference(Group.class, grp.getGroupName());
            currentGrp.setGroupMembers(userList);
        }
        currentCustomer.setUser(currentUser);

        em.merge(currentUser);
        em.merge(currentCustomer);
    }

    /**
     * Create Customer login information
     *
     * @param group
     * @param newUser
     */
    public void createCustomer(Group group, User newUser) {
        CustomersLogin customer = new CustomersLogin();
        newUser.setPassword("12345");
        newUser.addUserToGroup(group);
        em.persist(newUser);
        customer.setUser(newUser);
        em.persist(customer);
    }

}
