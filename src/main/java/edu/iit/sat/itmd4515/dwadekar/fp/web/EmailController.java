/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.web;

import edu.iit.sat.itmd4515.dwadekar.fp.cdi.EmailBean;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Used for sending Email to respective Email ID
 *
 * @author dwadekar
 */
@Named
@RequestScoped
public class EmailController extends AbstractJSFController {

    private static final Logger LOG = Logger.getLogger(LoginController.class.getName());

    private String firstName;
    private String lastName;
    private String emailAddress;

    @Inject
    EmailBean emailBean;

    /**
     * Default Constructor
     */
    public EmailController() {
    }

    /**
     * Post Construct Method
     */
    @Override
    @PostConstruct
    protected void postConstruct() {
        super.postConstruct();
    }

    /**
     * Redirecting to Subscribe mail operation
     *
     * @return
     */
    public String redirectSubscribe() {
        return "subscribeNews.xhtml";
    }

    /**
     * Redirect to Index page of a customer
     *
     * @return
     */
    public String redirectToIndexPage() {
        return "indexPage.xhtml";
    }

    /**
     * Method to send an Email to entered Email address
     *
     * @return
     */
    public String doSendMail() {
        LOG.info("Sending mail to " + firstName);
        LOG.info("Sending mail on email " + emailAddress);

        String subject = "News from Nexus Banking Group";
        String body = "Hi " + firstName + ", Welcome to Nexus Banking group!!!";

        emailBean.sendMail(emailAddress, subject, body);

        return "success.xhtml";
    }

    /**
     * Returns the first name of subscriber
     *
     * @return
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Set the first name of subscriber
     *
     * @param firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Returns the last name of subscriber
     *
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Set the last name of subscriber
     *
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Returns the Email Address of subscriber
     *
     * @return
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Set the Email Address of subscriber
     *
     * @param emailAddress
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

}
