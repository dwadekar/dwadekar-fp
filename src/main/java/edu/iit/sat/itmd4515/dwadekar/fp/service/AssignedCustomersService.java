/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.service;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.AssignedCustomers;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.RelationshipManager;
import java.util.List;
import javax.ejb.Stateless;

/**
 * Used to store information of list of Customers to whom Relationship Managers
 * are assigned
 *
 * @author dwadekar
 */
@Stateless
public class AssignedCustomersService extends AbstractService<AssignedCustomers> {

    /**
     * Default Constructor
     */
    public AssignedCustomersService() {
        super(AssignedCustomers.class);
    }

    /**
     * Returns the all information about assigned Customers
     *
     * @return
     */
    @Override
    public List<AssignedCustomers> findAll() {
        return em.createNamedQuery("AssignedCustomers.findAll", AssignedCustomers.class).getResultList();
    }

    /**
     * Delete the customer information sent as parameters
     *
     * @param assignCustomer
     * @param manager
     */
    public void deleteCustomer(AssignedCustomers assignCustomer, RelationshipManager manager) {
        AssignedCustomers currentCustomer = em.getReference(AssignedCustomers.class, assignCustomer.getId());
        manager = em.getReference(RelationshipManager.class, manager.getId());

        manager.getCustomers().remove(currentCustomer);

        em.remove(currentCustomer);
    }
}
