/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.web;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotNull;

/**
 * Used for controlling Login into application
 *
 * @author dwadekar
 */
@Named
@RequestScoped
public class LoginController extends AbstractJSFController {

    private static final Logger LOG = Logger.getLogger(LoginController.class.getName());

    @NotNull(message = "Please enter the username")
    private String username;
    @NotNull(message = "Please enter the password")
    private String password;

    /**
     * Default Constructor
     */
    public LoginController() {
    }

    /**
     * Post Construct Method
     */
    @Override
    @PostConstruct
    protected void postConstruct() {
        super.postConstruct();
    }

    /**
     * Returns the User name
     *
     * @return
     */
    public String getRemoteUser() {
        return facesContext.getExternalContext().getRemoteUser();
    }

    /**
     * Check whether Administrator user is logged in
     *
     * @return
     */
    public boolean isAdmin() {
        return facesContext.getExternalContext().isUserInRole("ADMINS");
    }

    /**
     * Check whether Employee user is logged in
     *
     * @return
     */
    public boolean isEmployee() {
        return facesContext.getExternalContext().isUserInRole("EMPLOYEE");
    }

    /**
     * Check whether Customer user is logged in
     *
     * @return
     */
    public boolean isCustomer() {
        return facesContext.getExternalContext().isUserInRole("CUSTOMER");
    }

    /**
     * Check the role of user
     *
     * @param path
     * @return
     */
    public String getRoleContextPath(String path) {
        if (isAdmin()) {
            return "/admin/" + path + FACES_REDIRECT;
        } else if (isEmployee()) {
            return "/employeePortal/" + path + FACES_REDIRECT;
        } else if (isCustomer()) {
            return "/customerPortal/" + path + FACES_REDIRECT;
        }

        return path;
    }

    /**
     * Method to do login into application
     *
     * @return
     */
    public String doLogin() {
        HttpServletRequest req = (HttpServletRequest) facesContext.getExternalContext().getRequest();
        try {
            req.login(username, password);
        } catch (ServletException ex) {
            LOG.log(Level.SEVERE, "Failed login from " + username);
            facesContext.addMessage(null, new FacesMessage("Bad Login", "You have entered a bad username or password."));
            return "login.xhtml";
        }
        return getRoleContextPath("welcome.xhtml");
    }

    /**
     * Method to do logout into application
     *
     * @return
     */
    public String doLogout() {
        HttpServletRequest req = (HttpServletRequest) facesContext.getExternalContext().getRequest();
        try {
            req.logout();
        } catch (ServletException ex) {
            LOG.log(Level.SEVERE, "Failed login from " + username);
            facesContext.addMessage(null, new FacesMessage("Bad Logout"));
            return "login.xhtml";
        }
        return "/login.xhtml";
    }

    /**
     * Method to redirect to login page
     *
     * @return
     */
    public String redirectLogin() {
        return "login.xhtml";
    }

    /**
     * Returns the username
     *
     * @return
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set the username
     *
     * @param username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Returns the password of user
     *
     * @return
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the password of user
     *
     * @param password
     */
    public void setPassword(String password) {
        this.password = password;
    }

}
