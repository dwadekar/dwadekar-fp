/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Abstract service for all services in project
 *
 * @author dwadekar
 * @param <T>
 */
public abstract class AbstractService<T> {

    /**
     * Set the Persistence Unit Used for project
     */
    @PersistenceContext(unitName = "dwadekarPU")
    protected EntityManager em;

    private Class<T> entityClass;

    /**
     * Parameterized Constructor
     *
     * @param entityClass
     */
    public AbstractService(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    /**
     * Used to create a record in database
     *
     * @param entity
     */
    public void create(T entity) {
        em.persist(entity);
    }

    /**
     * Used to find a record in database
     *
     * @param id
     * @return
     */
    public T find(Object id) {
        return em.find(entityClass, id);
    }

    /**
     * Used to update the information in database
     *
     * @param entity
     */
    public void update(T entity) {
        em.merge(entity);
    }

    /**
     * Used to delete the record in database
     *
     * @param entity
     */
    public void delete(T entity) {
        em.remove(entity);
    }

    /**
     * Used to find all database contents
     *
     * @return
     */
    public abstract List<T> findAll();

}
