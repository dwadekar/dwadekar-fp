/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.service;

import edu.iit.sat.itmd4515.dwadekar.fp.security.Group;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

/**
 * Used for proving services for different groups present in application
 *
 * @author dwadekar
 */
@Stateless
public class GroupService extends AbstractService<Group> {

    /**
     * Default Constructor
     */
    public GroupService() {
        super(Group.class);
    }

    @Override
    public List<Group> findAll() {
        return em.createNamedQuery("Group.findAll").getResultList();
    }

    /**
     * Returns the all information about group sent as parameter
     *
     * @param groupname
     * @return
     */
    public Group findByGroupName(String groupname) {
        TypedQuery<Group> query = em.createNamedQuery("Group.findByGroupname", Group.class);
        query.setParameter("groupname", groupname);
        return query.getSingleResult();
    }
}
