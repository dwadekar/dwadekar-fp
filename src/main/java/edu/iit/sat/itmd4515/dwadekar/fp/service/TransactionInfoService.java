/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.service;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.AccountInfo;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.TransactionInfo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;

/**
 * Used to provide various transaction information for a Customer
 *
 * @author dwadekar
 */
@Stateless
public class TransactionInfoService extends AbstractService<TransactionInfo> {

    /**
     * Default Constructor
     */
    public TransactionInfoService() {
        super(TransactionInfo.class);
    }

    /**
     * Returns the all information about Transactions done by Customers
     *
     * @return
     */
    @Override
    public List<TransactionInfo> findAll() {
        return em.createNamedQuery("TransactionInfo.findAll", TransactionInfo.class).getResultList();
    }

    /**
     * Create transaction in customer's mention account
     *
     * @param txn
     * @param account
     * @return 
     */
    public AccountInfo create(TransactionInfo txn, AccountInfo account) {
        List<TransactionInfo> transactions = new ArrayList<>();
        AccountInfo currentAccount = em.getReference(AccountInfo.class, account.getId());
        transactions = currentAccount.getTransactions();

        txn.setAccountNo(currentAccount.getAccountNo());
        txn.setCustomerID(currentAccount.getCustomerID());
        txn.setTxnDate(new Date());
        txn.setTypeOfTxn("D");
        currentAccount.setAvailableBalance((currentAccount.getAvailableBalance() - txn.getAmount()));
        transactions.add(txn);

        currentAccount.setTransactions(transactions);
        em.persist(txn);
        
        return currentAccount;
    }
    
    /**
     * Create transaction in customer's account
     * Transaction is perform by Employee of Bank
     *
     * @param txn
     * @param account
     * @return 
     */
    public AccountInfo createTxn(TransactionInfo txn, AccountInfo account){
        List<TransactionInfo> transactions = new ArrayList<>();
        AccountInfo currentAccount = em.getReference(AccountInfo.class, account.getId());
        transactions = currentAccount.getTransactions();
        
        txn.setAccountNo(currentAccount.getAccountNo());
        txn.setCustomerID(currentAccount.getCustomerID());
        txn.setTxnDate(new Date());
        if(txn.getTypeOfTxn().equals("D")){
            currentAccount.setAvailableBalance((currentAccount.getAvailableBalance() - txn.getAmount()));
        }else if(txn.getTypeOfTxn().equals("C")){
            currentAccount.setAvailableBalance((currentAccount.getAvailableBalance() + txn.getAmount()));
        }
        transactions.add(txn);
        
        currentAccount.setTransactions(transactions);
        em.persist(txn);
        
        return currentAccount;
    }
}
