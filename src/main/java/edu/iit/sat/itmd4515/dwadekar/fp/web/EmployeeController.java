/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.web;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.AccountInfo;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.Address;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.AssignedCustomers;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.Contact;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.Customer;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.ProofOfDocuments;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.RelationshipManager;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.TransactionInfo;
import edu.iit.sat.itmd4515.dwadekar.fp.service.AccountInfoService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.AddressService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.AssignedCustomersService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.BankService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.ContactService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.ProofOfDocService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.RelationshipManagerService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.TransactionInfoService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Used for Controlling Employee's operations
 *
 * @author dwadekar
 */
@Named
@RequestScoped
public class EmployeeController extends AbstractJSFController {

    private static final Logger LOG = Logger.getLogger(CustomerInfoController.class.getName());

    private List<Customer> customersList;
    private Customer custInfo;
    private Address addressInfo;
    private Contact contactInfo;
    private ProofOfDocuments docsProof;
    private AccountInfo accountInfo;
    private AccountInfo newAccountInfo;
    private RelationshipManager relationshipManagerInfo;
    private AssignedCustomers assignCustomer;
    private TransactionInfo txnByEmp;
    private List<AccountInfo> accountsList;
    private List<TransactionInfo> transactionList;
    private List<AssignedCustomers> assignedCustomersList;

    @EJB
    private BankService customerService;
    @EJB
    private AddressService addressService;
    @EJB
    private ContactService contactService;
    @EJB
    private ProofOfDocService proofDocService;
    @EJB
    private AccountInfoService accountInfoService;
    @EJB
    private TransactionInfoService txnInfoService;
    @EJB
    private RelationshipManagerService relationshipManagerService;
    @EJB
    private AssignedCustomersService assignedCustomersService;
    @Inject
    LoginController loginController;

    /**
     * Default Constructor
     */
    public EmployeeController() {
    }

    /**
     * Post Constructor
     */
    @Override
    @PostConstruct
    protected void postConstruct() {
        custInfo = new Customer();
        addressInfo = new Address();
        contactInfo = new Contact();
        docsProof = new ProofOfDocuments();
        accountInfo = new AccountInfo();
        txnByEmp = new TransactionInfo();
        transactionList = new ArrayList<>();
        try {
            customersList = customerService.findAll();
        } catch (NullPointerException ex) {
            LOG.info("No customer records found!! Null pointer exception caught..");
        }
        try {
            accountsList = accountInfoService.findAll();
        } catch (NullPointerException ex) {
            LOG.info("No account information records found!! Null pointer exception caught..");
        }
        relationshipManagerInfo = relationshipManagerService.findManager(loginController.getRemoteUser());
        try {
            assignedCustomersList = relationshipManagerService.findManager(loginController.getRemoteUser()).getCustomers();
        } catch (NullPointerException ex) {
            LOG.info("No assigned customers records found!! Null pointer exception caught..");
        }
        assignCustomer = new AssignedCustomers();
        super.postConstruct();
    }

    private void refreshCustomersList() {
        customersList = customerService.findAll();
    }

    private void refreshAccountList() {
        accountsList = accountInfoService.findAll();
    }

    private void refreshManagerList() {
        relationshipManagerInfo = relationshipManagerService.findManager(loginController.getRemoteUser());
        assignedCustomersList = relationshipManagerInfo.getCustomers();
    }

    /**
     * Display the Customer information
     *
     * @param custInfo
     * @return
     */
    public String doShowCustomerInfo(Customer custInfo) {
        LOG.info("Preparing to display Customer info " + custInfo.toString());
        this.custInfo = custInfo;
        addressInfo = this.custInfo.getAddress();
        contactInfo = this.custInfo.getContact();
        docsProof = this.custInfo.getDocumentProof();
        return "viewCustomerInfo";
    }

    /**
     * Update the selected Customer information
     *
     * @param custInfo
     * @return
     */
    public String doUpdateCustomer(Customer custInfo) {
        LOG.info("Preparing to update " + custInfo.toString());
        this.custInfo = custInfo;
        this.addressInfo = custInfo.getAddress();
        this.contactInfo = custInfo.getContact();
        this.docsProof = custInfo.getDocumentProof();
        return "editCustomer";
    }

    /**
     * Execute update operation for selected Customer
     *
     * @return
     */
    public String executeUpdateCustomer() {
        LOG.info("Preparing to update " + custInfo.toString());

        customerService.update(custInfo);
        refreshCustomersList();
        return "welcome";
    }

    /**
     * Delete the selected Customer information
     *
     * @param custInfo
     * @return
     */
    public String doDeleteCustomer(Customer custInfo) {
        LOG.info("Preparing to delete customer " + custInfo.toString());

        customerService.delete(custInfo);
        refreshCustomersList();
        return "success";
    }

    /**
     * Create new Customer information
     *
     * @return
     */
    public String doCreateCustomer() {
        LOG.info("Preparing to create a new customer");
        custInfo = new Customer();
        addressInfo = new Address();
        contactInfo = new Contact();
        docsProof = new ProofOfDocuments();
        return "createCustomer";
    }

    /**
     * Execute the create operation for the entered Customer information
     *
     * @return
     */
    public String executeCreateCustomer() {
        LOG.info("Preparing to create customer" + custInfo.toString());
        LOG.info("Preparing to create address" + addressInfo.toString());
        docsProof.setSubmittedDate(new Date());
        proofDocService.create(docsProof);

        addressService.create(addressInfo);
        contactService.create(contactInfo);

        custInfo.setContact(contactInfo);
        custInfo.setDocumentProof(docsProof);
        custInfo.setAddress(addressInfo);
        custInfo.setCreationDate(new Date());
        customerService.create(custInfo);

        refreshCustomersList();
        return "successCustomer";
    }

    /**
     * Display the Account information
     *
     * @param accountInfo
     * @return
     */
    public String doShowAccountInfo(AccountInfo accountInfo) {
        LOG.info("Preparing to display Account info " + accountInfo.toString());

        this.accountInfo = accountInfo;
        this.transactionList = accountInfo.getTransactions();
        return "viewAccountInfo";
    }

    /**
     * Delete the account information of a selected customer
     *
     * @param accountInfo
     * @return
     */
    public String doDeleteAccount(AccountInfo accountInfo) {
        LOG.info("Preparing to delete Account info " + accountInfo.toString());
        //this.accountInfo = accountInfo;
        accountInfoService.deleteAccount(accountInfo);
        refreshAccountList();
        return "successAccountDeletion";
    }

    /**
     * Update the selected account information
     *
     * @param accountInfo
     * @return
     */
    public String doUpdateAccount(AccountInfo accountInfo) {
        LOG.info("Preparing to update Account info " + accountInfo.toString());

        this.accountInfo = accountInfo;
        return "editAccount";
    }

    /**
     * Execute the update operation for selected account
     *
     * @return
     */
    public String executeUpdateAccount() {
        LOG.info("Preparing to update new Account info " + accountInfo.toString());

        accountInfoService.updateAccountInfo(accountInfo);
        refreshAccountList();
        return "successEditAccount";
    }

    /**
     * Perform transaction for selected account information
     *
     * @param accountInfo
     * @return
     */
    public String doPerformTransaction(AccountInfo accountInfo) {
        LOG.info("Preparing to perform txn in Account info " + accountInfo.toString());

        this.accountInfo = accountInfo;
        txnByEmp = new TransactionInfo();
        return "performTxn";
    }

    /**
     * Execute the create operation for transaction in selected Account
     *
     * @return
     */
    public String executeTransaction() {
        LOG.info("Preparing to insert txn info " + txnByEmp.toString());
        Float chkBalance = accountInfo.getAvailableBalance() - txnByEmp.getAmount();

        if (txnByEmp.getTypeOfTxn().equals("D")) {
            if (chkBalance < 0) {
                LOG.info("Sorry!! Balance in account will get lower than zero due to amount " + txnByEmp.getAmount());
                return "errorTxn";
            } else {
                LOG.info("Preparing to DEBIT Account " + accountInfo.getAccountNo());
                newAccountInfo = txnInfoService.createTxn(txnByEmp, accountInfo);
                return "successTxn";
            }
        } else {
            LOG.info("Preparing to CREDIT Account " + accountInfo.getAccountNo());
            newAccountInfo = txnInfoService.createTxn(txnByEmp, accountInfo);
            return "successTxn";
        }
    }

    /**
     * Create Account of a customer
     *
     * @param custInfo
     * @return
     */
    public String doCreateAccount(Customer custInfo) {
        LOG.info("Preparing to create new Account for " + custInfo.toString());
        long randomNum = Math.abs(new Random().nextLong());
        this.custInfo = custInfo;
        accountInfo = new AccountInfo();
        txnByEmp = new TransactionInfo();
        accountInfo.setCustomerID(this.custInfo.getId());
        accountInfo.setAccountNo(randomNum);
        txnByEmp.setCustomerID(this.custInfo.getId());
        //txnByEmp.setAccountNo(randomNum);
        txnByEmp.setModeOfTxn("Cash Deposit");
        txnByEmp.setTypeOfTxn("C");

        return "createAccountInfo";
    }

    /**
     * Execute the create operation
     *
     * @return
     */
    public String executeCreateAccount() {
        LOG.info("Preparing to create new Account for " + accountInfo.toString());

        txnByEmp.setCustomerID(accountInfo.getCustomerID());
        txnByEmp.setAccountNo(accountInfo.getAccountNo());
        txnByEmp.setAmount(accountInfo.getAvailableBalance());
        txnByEmp.setTxnDate(new Date());
        LOG.info("Preparing to create new Txn " + txnByEmp.toString());

        txnInfoService.create(txnByEmp);
        accountInfoService.createAccount(accountInfo, txnByEmp);
        refreshAccountList();
        return "successCreateAccount";
    }

    /**
     * Display assigned customers list to Employee
     *
     * @param assignCustomer
     * @return
     */
    public String doShowAssignedCustomer(AssignedCustomers assignCustomer) {
        LOG.info("Preparing to display assigned Customer " + assignCustomer.toString());

        this.assignCustomer = assignCustomer;
        return "viewAssignCustomer";
    }

    /**
     * Update the assigned customer information
     *
     * @param assignCustomer
     * @return
     */
    public String doUpdateAssignedCustomer(AssignedCustomers assignCustomer) {
        LOG.info("Preparing to update assigned Customer " + assignCustomer.toString());

        this.assignCustomer = assignCustomer;
        return "editAssignCustomer";
    }

    /**
     * Execute update assigned customer operation
     *
     * @return
     */
    public String executeUpdateAssignedCustomer() {
        LOG.info("Preparing to execute updated assigned Customer info " + assignCustomer.toString());

        assignedCustomersService.update(assignCustomer);
        refreshManagerList();
        return "successUpdateAssignCustomer";
    }

    /**
     * Delete the assigned customer to Employee
     *
     * @param assignCustomer
     * @return
     */
    public String doDeleteAssignCustomer(AssignedCustomers assignCustomer) {
        LOG.info("Preparing to delete assigned Customer " + assignCustomer.toString());
        this.assignCustomer = assignCustomer;

        assignedCustomersService.deleteCustomer(this.assignCustomer, relationshipManagerInfo);
        refreshManagerList();
        return "successDeleteAssignCustomer";
    }

    /**
     * Returns the list of Customer's
     *
     * @return
     */
    public List<Customer> getCustomersList() {
        return customersList;
    }

    /**
     * Set the list of Customer's
     *
     * @param customersList
     */
    public void setCustomersList(List<Customer> customersList) {
        this.customersList = customersList;
    }

    /**
     * Returns the customer information
     *
     * @return
     */
    public Customer getCustInfo() {
        return custInfo;
    }

    /**
     * Set the customer information
     *
     * @param custInfo
     */
    public void setCustInfo(Customer custInfo) {
        this.custInfo = custInfo;
    }

    /**
     * Returns the address of a customer
     *
     * @return
     */
    public Address getAddressInfo() {
        return addressInfo;
    }

    /**
     * Set the address of a customer
     *
     * @param addressInfo
     */
    public void setAddressInfo(Address addressInfo) {
        this.addressInfo = addressInfo;
    }

    /**
     * Returns the contact of a customer
     *
     * @return
     */
    public Contact getContactInfo() {
        return contactInfo;
    }

    /**
     * Set the Contact information of a customer
     *
     * @param contactInfo
     */
    public void setContactInfo(Contact contactInfo) {
        this.contactInfo = contactInfo;
    }

    /**
     * Returns the proof of document submitted by customer
     *
     * @return
     */
    public ProofOfDocuments getDocsProof() {
        return docsProof;
    }

    /**
     * Set the proof of document submitted by customer
     *
     * @param docsProof
     */
    public void setDocsProof(ProofOfDocuments docsProof) {
        this.docsProof = docsProof;
    }

    /**
     * Returns the list of account information of a customer's
     *
     * @return
     */
    public List<AccountInfo> getAccountsList() {
        return accountsList;
    }

    /**
     * Set the list of account information of a customer's
     *
     * @param accountsList
     */
    public void setAccountsList(List<AccountInfo> accountsList) {
        this.accountsList = accountsList;
    }

    /**
     * Returns the account information of a customer
     *
     * @return
     */
    public AccountInfo getAccountInfo() {
        return accountInfo;
    }

    /**
     * Set the account information of a customer
     *
     * @param accountInfo
     */
    public void setAccountInfo(AccountInfo accountInfo) {
        this.accountInfo = accountInfo;
    }

    /**
     * Returns the list of Transactions perform by customer
     *
     * @return
     */
    public List<TransactionInfo> getTransactionList() {
        return transactionList;
    }

    /**
     * Set the list of Transactions perform by customer
     *
     * @param transactionList
     */
    public void setTransactionList(List<TransactionInfo> transactionList) {
        this.transactionList = transactionList;
    }

    /**
     * Returns Transactions perform by Employee
     *
     * @return
     */
    public TransactionInfo getTxnByEmp() {
        return txnByEmp;
    }

    /**
     * Set Transactions perform by Employee
     *
     * @param txnByEmp
     */
    public void setTxnByEmp(TransactionInfo txnByEmp) {
        this.txnByEmp = txnByEmp;
    }

    /**
     * Returns the assigned Manager information
     *
     * @return
     */
    public RelationshipManager getRelationshipManagerInfo() {
        return relationshipManagerInfo;
    }

    /**
     * Set the assigned Manager information
     *
     * @param relationshipManagerInfo
     */
    public void setRelationshipManagerInfo(RelationshipManager relationshipManagerInfo) {
        this.relationshipManagerInfo = relationshipManagerInfo;
    }

    /**
     * Returns the list of Assigned customer's
     *
     * @return
     */
    public List<AssignedCustomers> getAssignedCustomersList() {
        return assignedCustomersList;
    }

    /**
     * Set the list of Assigned customer's
     *
     * @param assignedCustomersList
     */
    public void setAssignedCustomersList(List<AssignedCustomers> assignedCustomersList) {
        this.assignedCustomersList = assignedCustomersList;
    }

    /**
     * Returns Assigned customer information
     *
     * @return
     */
    public AssignedCustomers getAssignCustomer() {
        return assignCustomer;
    }

    /**
     * Set Assigned customer information
     *
     * @param assignCustomer
     */
    public void setAssignCustomer(AssignedCustomers assignCustomer) {
        this.assignCustomer = assignCustomer;
    }

    /**
     * Returns Account information
     *
     * @return
     */
    public AccountInfo getNewAccountInfo() {
        return newAccountInfo;
    }

    /**
     * Set newly changed Account information
     *
     * @param newAccountInfo
     */
    public void setNewAccountInfo(AccountInfo newAccountInfo) {
        this.newAccountInfo = newAccountInfo;
    }

}
