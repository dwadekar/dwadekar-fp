/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.service;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.Employee;
import edu.iit.sat.itmd4515.dwadekar.fp.security.Group;
import edu.iit.sat.itmd4515.dwadekar.fp.security.User;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

/**
 * Used for providing Employee Services to Employee Group
 *
 * @author dwadekar
 */
@Stateless
public class EmployeeService extends AbstractService<Employee> {

    /**
     * Default Constructor
     */
    public EmployeeService() {
        super(Employee.class);
    }

    /**
     * Returns the list of all Employee's login information
     *
     * @return
     */
    @Override
    public List<Employee> findAll() {
        return em.createNamedQuery("Employee.findAll").getResultList();
    }

    /**
     * Return the information of Single Employee by username
     *
     * @param username
     * @return
     */
    public Employee findByUsername(String username) {
        TypedQuery<Employee> query = em.createNamedQuery("Employee.findByUsername", Employee.class);
        query.setParameter("username", username);
        return query.getSingleResult();
    }

    /**
     * Delete Employee login information
     *
     * @param employee
     */
    public void deleteEmployee(Employee employee) {
        employee = em.getReference(Employee.class, employee.getId());
        User user = employee.getUser();
        List<Group> group = user.getUserGroups();
        for (Group grp : group) {
            Group currentGrp = em.getReference(Group.class, grp.getGroupName());
            currentGrp.getGroupMembers().remove(user);
        }

        em.remove(employee);
        em.remove(user);
    }

    /**
     * Update Employee login information
     *
     * @param newEmployee
     * @param newUser
     */
    public void updateEmployee(Employee newEmployee, User newUser) {
        Employee currentEmployee = em.getReference(Employee.class, newEmployee.getId());
        User currentUser = em.getReference(User.class, newUser.getUserName());
        List<Group> group = currentUser.getUserGroups();
        currentUser.setPassword("12345");
        currentUser.setNameOfUser(newUser.getNameOfUser());
        currentUser.setEmailID(newUser.getEmailID());
        List<User> userList = new ArrayList<>();
        userList.add(currentUser);
        for (Group grp : group) {
            Group currentGrp = em.getReference(Group.class, grp.getGroupName());
            currentGrp.setGroupMembers(userList);
        }
        currentEmployee.setUser(currentUser);

        em.merge(currentUser);
        em.merge(currentEmployee);
    }

    /**
     * Create Employee login information
     *
     * @param group
     * @param newUser
     */
    public void createEmployee(Group group, User newUser) {
        Employee employee = new Employee();
        newUser.setPassword("12345");
        newUser.addUserToGroup(group);
        em.persist(newUser);
        employee.setUser(newUser);
        em.persist(employee);
    }

}
