/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.service;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.Address;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.Contact;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.Customer;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.ProofOfDocuments;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;

/**
 * Class used for Bank Services
 *
 * @author dwadekar
 */
@Stateless
public class BankService extends AbstractService<Customer> {

    /**
     * Default COnstructor
     */
    public BankService() {
        super(Customer.class);
    }

    /**
     * Returns the all Customer information
     *
     * @return
     */
    @Override
    public List<Customer> findAll() {
        return em.createNamedQuery("Customer.findAll", Customer.class).getResultList();
    }

    /**
     * Returns the Customer information searched by Username
     *
     * @param username
     * @return
     */
    public Customer findCustomer(String username) {
        Customer cust;
        try {
            cust = em.createNamedQuery("Customer.findByName", Customer.class).setParameter("name", username).getSingleResult();
        } catch (NoResultException ex) {
            cust = null;
        }
        return cust;
    }

    /**
     * Update the customer information
     *
     * @param newCustInfo
     */
    public void update(Customer newCustInfo) {
        Customer currentCustInfo = em.getReference(Customer.class, newCustInfo.getId());
        Address currentAddresInfo = currentCustInfo.getAddress();
        Contact currentContactInfo = currentCustInfo.getContact();
        ProofOfDocuments currentDocsProof = currentCustInfo.getDocumentProof();

        currentCustInfo.setId(newCustInfo.getId());
        currentCustInfo.setCreationDate(newCustInfo.getCreationDate());
        currentCustInfo.setTitle(newCustInfo.getTitle());
        currentCustInfo.setFirstName(newCustInfo.getFirstName());
        currentCustInfo.setMiddleName(newCustInfo.getMiddleName());
        currentCustInfo.setLastName(newCustInfo.getLastName());
        currentCustInfo.setRelationShipType(newCustInfo.getRelationShipType());
        currentCustInfo.setBranchId(newCustInfo.getBranchId());
        currentCustInfo.setDateOfBirth(newCustInfo.getDateOfBirth());
        currentCustInfo.setAddress(currentAddresInfo);
        currentCustInfo.setContact(currentContactInfo);
        currentCustInfo.setDocumentProof(currentDocsProof);

        em.merge(currentCustInfo);

    }

    /**
     * Delete the customer information
     *
     * @param custInfo
     */
    public void delete(Customer custInfo) {
        Customer currentCustInfo = em.getReference(Customer.class, custInfo.getId());
        Address currentAddresInfo = em.getReference(Address.class, custInfo.getAddress().getId());
        Contact currentContactInfo = em.getReference(Contact.class, custInfo.getContact().getId());;
        ProofOfDocuments currentDocsProof = currentCustInfo.getDocumentProof();

        em.remove(currentCustInfo);
        em.remove(currentAddresInfo);
        em.remove(currentContactInfo);

        em.remove(currentDocsProof);

    }

    /**
     * Create user information
     *
     * @param custInfo
     * @param addressInfo
     */
    public void create(Customer custInfo, Address addressInfo) {
        custInfo.setAddress(addressInfo);

        //em.create(custInfo);
    }
}
