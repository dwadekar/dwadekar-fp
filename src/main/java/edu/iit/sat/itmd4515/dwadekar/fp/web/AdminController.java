/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.web;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.Admin;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.CustomersLogin;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.Employee;
import edu.iit.sat.itmd4515.dwadekar.fp.security.Group;
import edu.iit.sat.itmd4515.dwadekar.fp.security.User;
import edu.iit.sat.itmd4515.dwadekar.fp.service.AdminService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.CustomerService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.EmployeeService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.GroupService;
import edu.iit.sat.itmd4515.dwadekar.fp.service.UserService;
import java.util.List;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 * Used for Controlling Administrator's operations
 *
 * @author dwadekar
 */
@Named
@RequestScoped
public class AdminController extends AbstractJSFController {

    private static final Logger LOG = Logger.getLogger(CustomerInfoController.class.getName());

    private Admin adminUser;
    private User user;
    private Group group;
    private Employee employee;
    private CustomersLogin customer;
    private List<Employee> employeeList;
    private List<CustomersLogin> customersList;
    private List<Admin> adminList;
    private List<Group> groupList;
    private List<User> userList;

    @EJB
    private EmployeeService employeeService;
    @EJB
    private CustomerService customerService;
    @EJB
    private AdminService adminService;
    @EJB
    private GroupService groupService;
    @EJB
    private UserService userService;
    @Inject
    LoginController loginController;

    /**
     * Default Constructor
     */
    public AdminController() {
    }

    /**
     * Post Constructor
     */
    @Override
    @PostConstruct
    protected void postConstruct() {
        employee = new Employee();
        customer = new CustomersLogin();
        adminUser = new Admin();
        user = new User();
        group = new Group();
        try {
            employeeList = employeeService.findAll();
        } catch (NullPointerException ex) {
            LOG.info("No employee login records found!! Null pointer exception caught..");
        }
        try {
            customersList = customerService.findAll();
        } catch (NullPointerException ex) {
            LOG.info("No customer login records found!! Null pointer exception caught..");
        }
        try {
            adminList = adminService.findAll();
        } catch (NullPointerException ex) {
            LOG.info("No admin login records found!! Null pointer exception caught..");
        }
        try {
            groupList = groupService.findAll();
        } catch (NullPointerException ex) {
            LOG.info("No group records found!! Null pointer exception caught..");
        }
        super.postConstruct();
    }

    private void refreshEmployeeList() {
        employeeList = employeeService.findAll();
    }

    private void refreshCustomerList() {
        customersList = customerService.findAll();
    }

    private void refreshAdminList() {
        adminList = adminService.findAll();
    }

    /**
     * Used for displaying the Employee information
     *
     * @param employee
     * @return
     */
    public String doShowEmployee(Employee employee) {
        LOG.info("Preparing to display Employee info " + employee.getUser().getUserName());

        this.employee = employee;
        return "viewEmployee";
    }

    /**
     * Used for executing creation of Employee login information
     */
    public void executeCreateEmployeeLogin() {
        LOG.info("Preparing to create Employee login info " + user.getUserName());

        employeeService.createEmployee(group, user);
        refreshEmployeeList();
    }

    /**
     * Used for updating the Employee information
     *
     * @param employee
     * @return
     */
    public String doUpdateEmployee(Employee employee) {
        LOG.info("Preparing to update Employee info " + employee.getUser().getUserName());

        this.employee = employee;
        this.user = this.employee.getUser();
        return "editEmployeeLogin";
    }

    /**
     * Used for executing updating Employee login information
     *
     * @return
     */
    public String executeUpdateEmployee() {
        LOG.info("Preparing to update Employee info " + user.getUserName());

        employeeService.updateEmployee(employee, user);
        refreshEmployeeList();
        return "successUpdateEmployee";
    }

    /**
     * Used for deleting the Employee information
     *
     * @param employee
     * @return
     */
    public String doDeleteEmployee(Employee employee) {
        LOG.info("Preparing to delete Employee info " + employee.getUser().getUserName());

        this.employee = employee;
        employeeService.deleteEmployee(this.employee);
        refreshEmployeeList();
        return "successEmpDelete";
    }

    /**
     * Used for displaying the Customer login information
     *
     * @param customer
     * @return
     */
    public String doShowCustomer(CustomersLogin customer) {
        LOG.info("Preparing to display Customer info " + customer.getUser().getUserName());

        this.customer = customer;
        return "viewCustomer";
    }

    /**
     * Used for executing creation of Customer login information
     */
    public void executeCreateCustomerLogin() {
        LOG.info("Preparing to create Customer login info " + user.getUserName());

        customerService.createCustomer(group, user);
        refreshCustomerList();
    }

    /**
     * Used for updating the Customer login information
     *
     * @param customer
     * @return
     */
    public String doUpdateCustomer(CustomersLogin customer) {
        LOG.info("Preparing to update Customer info " + customer.getUser().getUserName());

        this.customer = customer;
        this.user = this.customer.getUser();
        return "editCustomerLogin";
    }

    /**
     * Used for executing updating Customer login information
     *
     * @return
     */
    public String executeUpdateCustomer() {
        LOG.info("Preparing to update Customer info " + user.getUserName());

        customerService.updateCustomer(customer, user);
        refreshCustomerList();
        return "successUpdateCustomer";
    }

    /**
     * Used for deleting the Customer information
     *
     * @param customer
     * @return
     */
    public String doDeleteCustomer(CustomersLogin customer) {
        LOG.info("Preparing to delete Customer info " + customer.getUser().getUserName());

        this.customer = customer;
        customerService.deleteCustomer(this.customer);
        refreshCustomerList();
        return "successCustDelete";
    }

    /**
     * Used for displaying the Administrator login information
     *
     * @param adminUser
     * @return
     */
    public String doShowAdmin(Admin adminUser) {
        LOG.info("Preparing to display Admin info " + adminUser.getUser().getUserName());

        this.adminUser = adminUser;
        return "viewAdmin";
    }

    /**
     * Used for executing creation of Administrator login information
     */
    public void executeCreateAdmin() {
        LOG.info("Preparing to create Administrator login info " + user.getUserName());

        adminService.createAdmin(group, user);
        refreshAdminList();
    }

    /**
     * Used for updating the Administrator login information
     *
     * @param adminUser
     * @return
     */
    public String doUpdateAdmin(Admin adminUser) {
        LOG.info("Preparing to update Admin info " + adminUser.getUser().getUserName());

        this.adminUser = adminUser;
        this.user = this.adminUser.getUser();
        return "editAdminLogin";
    }

    /**
     * Used for executing updating Administrator login information
     *
     * @return
     */
    public String executeUpdateAdmin() {
        LOG.info("Preparing to update Admin info " + user.getUserName());

        adminService.updateAdmin(adminUser, user);
        refreshAdminList();
        return "successUpdateAdmin";
    }

    /**
     * Used for deleting the Administrator information
     *
     * @param adminUser
     * @return
     */
    public String doDeleteAdmin(Admin adminUser) {
        LOG.info("Preparing to delete Customer info " + adminUser.getUser().getUserName());

        this.adminUser = adminUser;
        adminService.deleteAdmin(this.adminUser);
        refreshAdminList();
        return "successAdminDelete";
    }

    /**
     * Used for creation of user with different group's available
     *
     * @return
     */
    public String doCreateUser() {
        LOG.info("Preparing to create User Login Information");

        user = new User();
        groupList = groupService.findAll();
        return "createUserLogin";
    }

    /**
     * Used for executing creation of user required for login into system with
     * different available group's
     *
     * @return
     */
    public String executeCreateUser() {
        LOG.info("Preparing to create respective User Login Profile");
        String chkUser = "N";
        userList = userService.findAll();

        for (User currentUser : userList) {
            if (currentUser.getUserName().equalsIgnoreCase(user.getUserName())) {
                chkUser = "Y";
            }
        }

        if (chkUser.equals("Y")) {
            LOG.info("User profile already exists!!");
            return "errorCreateUser";
        } else {
            if (group.getGroupName().equalsIgnoreCase("Admin")) {
                LOG.info("Preparing to create Administrator Login Profile");
                executeCreateAdmin();
            } else if (group.getGroupName().equalsIgnoreCase("Employees")) {
                LOG.info("Preparing to create Employee Login Profile");
                executeCreateEmployeeLogin();
            } else if (group.getGroupName().equalsIgnoreCase("Customers")) {
                LOG.info("Preparing to create Administrator Login Profile");
                executeCreateCustomerLogin();
            }
            return "successCreateUser";
        }

    }

    /**
     * Returns the Administrator login details
     *
     * @return
     */
    public Admin getAdminUser() {
        return adminUser;
    }

    /**
     * Set the Administrator login details
     *
     * @param adminUser
     */
    public void setAdminUser(Admin adminUser) {
        this.adminUser = adminUser;
    }

    /**
     * Returns the User login details
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     * Set the User login details
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

    /**
     * Returns the details of Group in which user resides
     *
     * @return
     */
    public Group getGroup() {
        return group;
    }

    /**
     * Set the Group for the respective User
     *
     * @param group
     */
    public void setGroup(Group group) {
        this.group = group;
    }

    /**
     * Returns the Employee login information
     *
     * @return
     */
    public Employee getEmployee() {
        return employee;
    }

    /**
     * Set the Employee login information
     *
     * @param employee
     */
    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    /**
     * Returns the Customer login information
     *
     * @return
     */
    public CustomersLogin getCustomer() {
        return customer;
    }

    /**
     * Set the Customer login information
     *
     * @param customer
     */
    public void setCustomer(CustomersLogin customer) {
        this.customer = customer;
    }

    /**
     * Returns the list of Employee login information
     *
     * @return
     */
    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    /**
     * Set the list of Employee login information
     *
     * @param employeeList
     */
    public void setEmployeeList(List<Employee> employeeList) {
        this.employeeList = employeeList;
    }

    /**
     * Returns the list of Customer login information
     *
     * @return
     */
    public List<CustomersLogin> getCustomersList() {
        return customersList;
    }

    /**
     * Set the list of Customer login information
     *
     * @param customersList
     */
    public void setCustomersList(List<CustomersLogin> customersList) {
        this.customersList = customersList;
    }

    /**
     * Returns the list of Administrator group login information
     *
     * @return
     */
    public List<Admin> getAdminList() {
        return adminList;
    }

    /**
     * Set the list of Administrator group login information
     *
     * @param adminList
     */
    public void setAdminList(List<Admin> adminList) {
        this.adminList = adminList;
    }

    /**
     * Returns the list of available group's within application
     *
     * @return
     */
    public List<Group> getGroupList() {
        return groupList;
    }

    /**
     * Set the list of available group's within application
     *
     * @param groupList
     */
    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }

    /**
     * Returns the list of available user's within application
     *
     * @return
     */
    public List<User> getUserList() {
        return userList;
    }

    /**
     * Set the list of available user's within application
     *
     * @param userList
     */
    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

}
