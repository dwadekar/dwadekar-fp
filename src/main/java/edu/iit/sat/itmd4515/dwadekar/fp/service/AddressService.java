/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.service;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.Address;
import java.util.List;
import javax.ejb.Stateless;

/**
 * Used for providing Address service information of Customer
 *
 * @author dwadekar
 */
@Stateless
public class AddressService extends AbstractService<Address> {

    /**
     * Default Constructor
     */
    public AddressService() {
        super(Address.class);
    }

    /**
     * Returns the all information about Customer's Address
     *
     * @return
     */
    @Override
    public List<Address> findAll() {
        return em.createNamedQuery("Address.findAll", Address.class).getResultList();
    }
}
