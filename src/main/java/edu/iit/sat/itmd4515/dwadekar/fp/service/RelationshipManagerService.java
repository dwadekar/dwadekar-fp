/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.service;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.RelationshipManager;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;

/**
 * Used to assign Relationship Manager to Customer
 *
 * @author dwadekar
 */
@Stateless
public class RelationshipManagerService extends AbstractService<RelationshipManager> {

    /**
     * Default Constructor
     */
    public RelationshipManagerService() {
        super(RelationshipManager.class);
    }

    /**
     * Returns the all information about assigned Relationship Managers
     *
     * @return
     */
    @Override
    public List<RelationshipManager> findAll() {
        return em.createNamedQuery("RelationshipManager.findAll", RelationshipManager.class).getResultList();
    }

    /**
     * Returns the information Relationship Manager searched by Username
     *
     * @param username
     * @return
     */
    public RelationshipManager findManager(String username) {
        RelationshipManager rManager;
        try {
            rManager = em.createNamedQuery("RelationshipManager.findByName", RelationshipManager.class).setParameter("name", username).getSingleResult();
        } catch (NoResultException ex) {
            rManager = null;
        }
        return rManager;
    }
}
