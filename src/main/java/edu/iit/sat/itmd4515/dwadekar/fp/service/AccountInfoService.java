/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.service;

import edu.iit.sat.itmd4515.dwadekar.fp.domain.AccountInfo;
import edu.iit.sat.itmd4515.dwadekar.fp.domain.TransactionInfo;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;

/**
 * Used to provide account creation service to Customer
 *
 * @author dwadekar
 */
@Stateless
public class AccountInfoService extends AbstractService<AccountInfo> {

    /**
     * Default Constructor
     */
    public AccountInfoService() {
        super(AccountInfo.class);
    }

    /**
     * Returns the all information about accounts of various Customers
     *
     * @return
     */
    @Override
    public List<AccountInfo> findAll() {
        return em.createNamedQuery("AccountInfo.findAll", AccountInfo.class).getResultList();
    }
    
    /**
     * Returns the all information about accounts of particular Customer
     *
     * @param custID
     * @return
     */
    public List<AccountInfo> findAccounts(Long custID){
        List<AccountInfo> accountsList;
        accountsList = em.createNamedQuery("AccountInfo.findByID", AccountInfo.class).setParameter("custID", custID).getResultList();
        
        return accountsList;
    }

    /**
     * Delete the account of customer
     *
     * @param account
     */
    public void deleteAccount(AccountInfo account) {
        account = em.getReference(AccountInfo.class, account.getId());

        em.remove(account);
    }

    /**
     * Update the account information of customer
     *
     * @param newAccount
     */
    public void updateAccountInfo(AccountInfo newAccount) {
        List<TransactionInfo> txnList = new ArrayList<>();
        AccountInfo currentAccountInfo = em.getReference(AccountInfo.class, newAccount.getId());
        txnList = currentAccountInfo.getTransactions();

        currentAccountInfo.setId(newAccount.getId());
        currentAccountInfo.setCreationDate(newAccount.getCreationDate());
        currentAccountInfo.setCustomerID(newAccount.getCustomerID());
        currentAccountInfo.setAccountNo(newAccount.getAccountNo());
        currentAccountInfo.setAccountType(newAccount.getAccountType());
        currentAccountInfo.setAccountDescription(newAccount.getAccountDescription());
        currentAccountInfo.setCurrency(newAccount.getCurrency());
        currentAccountInfo.setAvailableBalance(newAccount.getAvailableBalance());
        currentAccountInfo.setTransactions(txnList);

        em.merge(currentAccountInfo);
    }

    /**
     * Create an account of a customer
     *
     * @param newAccount
     * @param txn
     */
    public void createAccount(AccountInfo newAccount, TransactionInfo txn) {
        List<TransactionInfo> txnList = new ArrayList<>();
        txnList.add(txn);

        newAccount.setTransactions(txnList);

        em.persist(newAccount);
    }
}
