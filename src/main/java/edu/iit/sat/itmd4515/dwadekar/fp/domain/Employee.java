/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.iit.sat.itmd4515.dwadekar.fp.domain;

import edu.iit.sat.itmd4515.dwadekar.fp.security.User;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 * Employee login information domain class
 *
 * @author dwadekar
 */
@Entity
@NamedQueries({
    @NamedQuery(name = "Employee.findAll", query = "select e from Employee e"),
    @NamedQuery(name = "Employee.findByUsername", query = "select e from Employee e where e.user.userName = :username")
})
public class Employee extends CommonEntity {

    @OneToOne
    @JoinColumn(name = "username")
    private User user;

    /**
     * Default Constructor
     */
    public Employee() {
    }

    /**
     * Used to get User information
     *
     * @return
     */
    public User getUser() {
        return user;
    }

    /**
     * Used to assign User information to User class
     *
     * @param user
     */
    public void setUser(User user) {
        this.user = user;
    }

}
